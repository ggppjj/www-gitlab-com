---
layout: markdown_page
title: "Hiring"
---
## Hiring Process

1. Create job description.
    * The CEO needs to authorize any new job positions/searches, and agree on the proposed hiring team.
      * A rough estimate of preferred range of compensation, or some other way of dealing with that inevitable interview question, needs to be determined.
    * The description will go on https://about.gitlab.com/jobs site, and will consist of:
      * Job title
      * Preferred geographic region where candidate should reside
      * Pre-amble about GitLab as a company (alternative: the pre-amble may be placed at the top of the jobs page and omitted from the individual descriptions).
      * Description of role
      * Requirements for the role (can be split into must-have’s and nice-to-have’s)
      * Post-amble stating how to apply, and who the hiring manager is.
1. Define hiring team.
    * Roles to be assigned are: (one person can of course handle multiple roles)
      * Person(s) to do first vetting of candidates, selecting applicants  for interview.
      * Person(s) to have (first round) interviews.
      * Optional: Person(s) to have second round interviews.
      * Person(s) to make final decision to make offer.
      * Person(s) actually making the offer, including terms of offer.
      * Person(s) to handle communications with applicants along the way.
1. Define hiring timeline.
    * Choose from specific deadlines (e.g. all applicants will be reviewed on date X, hear back by date Y), or
    * Choose rolling application process, wherein the review and interview process  happen as applications come in.
    * More typical: some combination of both. E.g. wait for X amount of time to gather enough applications, then review in bulk, set up first interviews, and repeat this process until a suitable applicant is found.
1. Publish the job description.
    * This is done on the GitLab jobs site, but only after the CEO (or person authorized by CEO) has signed off on the description, hiring team, and timeline.
1. Optional: advertise the job description.
    * This can be through “soft” referral, e.g. all GitLab staff post link to jobs site on their LinkedIn profiles.
    * And/Or it can be through job boards.
1. Interview Questions.
    * Hiring team to determine which questions need to be asked, and by whom in the team.
    * Homework assignments may be required for some positions.
1. Communication with Applicants
    * Applicants should receive confirmation of their application, thanking them for submitting their information. This may be an automated message.
    * If information is missing and the applicant seems sufficiently promising (or not enough information to be able to make that determination), the appropriate person from the hiring team should follow up requesting additional information.
    * Interviews should be set up in accordance with the defined hiring timeline; and applicants should be notified of this process as much as possible.
    * At time of interview, applicant should be told what the timeline is for a decision, and what the next steps are (if any).
1. Make a decision, make an offer.
    * The CEO needs to authorize offers.
    * Sign up successful applicant and move to onboarding.
    * Inform other applicants that we selected someone else this time. Applicants remain in the database and may be contacted in the future for other roles.


## Checklist for New Hires

Create issue for new hire in organization with following checklist.
This list looks strange in this handbook but this is to ensure you can copy paste it.
When you paste it in an issue it will have checkboxes that you can click to complete.

```
* [ ] Signed PIAA in Dropbox
* [ ] Signed contract in Dropbox
* [ ] Scan of photo id in Dropbox
* [ ] Complete TriNet steps/Enroll (https://docs.google.com/document/d/1osAcaVUKU6v3GbuenygxSMqlFBuX7IxF5dI4AaxdeQc/edit#heading=h.8vrvk3l6lfzl)
* [ ] Set up new hire training with TriNet
* [ ] Send [People Information sheet](https://docs.google.com/spreadsheets/d/1vpFL7pPD6DYg9bgnW9JOFUKTg1XkTs10yOOyB7KqWxM/edit#gid=0) and instruct to return answers via email or slack
* [ ] Create folder for new hire in Lastpass
* [ ] Put People Information into Profiles in Lastpass
* [ ] Create Google account, firstname@gitlab.com or initial(s)@gitlab.com, email instructions to private email address, comment with private email below this issue, turn off [2FA enforcement](https://admin.google.com/gitlab.com/AdminHome#ServiceSettings/notab=1&service=securitysetting&subtab=org) and schedule reenabling it
* [ ] Add to Slack
* [ ] Send an email to company address: 'Please read https://about.gitlab.com/handbook/ and the relevant onboarding pages that are linked from there and let me know if you have any questions.'
* [ ] [Add to Lastpass](https://lastpass.com/enterprise_create.php)
* [ ] Add to Recurly (https://app.recurly.com/login)
* [ ] Add Webex account (if necessary)
* [ ] Add to GitLab Dropbox
* [ ] Add to BV and/or Inc Dropbox (where/when approrpriate)
* [ ] [Add to Mailchimp](https://us5.admin.mailchimp.com/account/users/) (if sales or finance)
* [ ] Add to [QuickBooks users](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/handbook/hiring/index.md) (finance only)
* [ ] Add to Comerica (as user or viewer only if in Finance)
* [ ] Gitlab.com account invited to the [gitlab.com group](https://gitlab.com/groups/gitlab-com/group_members) as a developer
* [ ] Create a [new dev.GitLab.org account](https://dev.gitlab.org/admin/users/new) and invite to the [gitlab group](https://dev.gitlab.org/groups/gitlab/group_members) as a developer
* [ ] /cc the new user in the organization issue that was created for them.
* [ ] Invite to team meeting
* [ ] Invite to sales meeting
* [ ] Invite to autoconnect on Beamy
```

Please update this list as more steps arise.

## Entering New Hires into TriNet

Employer enters the employee data in the HR Passport with the information below

1. Under the My Staff tab- select new hire/rehire and a drop down menu will appear.

1. HR enters all of the necessary information:

    * Company name autopopulates
    * SS
    * Form of address for hire (Mr. Ms, etc.)
    * First name
    * Last name
    * Middle name or initial
    * Country
    * Address
    * Home phone
    * Home email
    * Gener
    * Ethnicity (you must select something - guess if employee declines to state)
    * Military status

At the bottom of the screen, select next

    * TriNet’s start date
    * Reason - drop down menu with options
    * Employment type - Full time or PT options
    * Select reg/temp bubble
    * Employee Class - drop down between regular and commission
    * Estimated annual wages (does not include anything besides base salary)
    * Benefit class
    * Future benefits class -
    * Standard Hours/week - Part time or Full time
    * Business Title - see org chart
    * Job Code - no need to enter anything here
    * FLSA status- drop down options are exempt, non-exempt, computer prof-non-exempt, computer prof- exempt
    * Supervisor - drop down menu of names
    * Compensation Basis
    * Compensation Rate
    * Departments
    * Work Location - drop down menu
    * Pay Group - only one option
    * Employee ID - not necessary
    * Work email
    * Grouping A/level - not necessary
    * Grouping B/sponsor- not necessary

Select next or save (if you select save, it will hold your information)

    * Vacation/PTO - drop down menu only provides one option- select this
    * Sick- drop down menu only provides one option- select this
    * Personal Time - leave blank
    * Floating Holidays - leave blank
    * Birthdate - mm/dd/yyyy
    * Workers compensation- select unknown and it will default to our principle class code for our industry
Window: Describe employees job duties - simple description

After submission -  you will receive a prompt for final submission, select and submit.

Note: if you save the information to finish at a later date, go to the Work Inbox and select New Hires Not Submitted to continue.

1. The employee receives a welcome email the night before their start date.

1. The employee is prompted to log on, complete tax withholding (W4 data), direct deposit information, section 1 of the I-9, and benefits election (if eligible).

1. The employer logs in to HR Passport and is prompted by way of work inbox item, to complete section 2 of the I-9.

